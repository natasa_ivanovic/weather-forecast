# weather-forecast

Node packet manager (npm) is required. 
(Link: https://www.npmjs.com/get-npm)

App running at:
  - Local:   http://localhost:8081/ 
  - Network: http://192.168.0.13:8081/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
