import Vue from 'vue'
import App from './App.vue'
import Overlay from './components/Overlay.vue'
import Toolbar from './components/Toolbar.vue'

Vue.component('app-overlay', Overlay);
Vue.component('app-toolbar', Toolbar);


import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(Chartkick.use(Chart))

Vue.config.productionTip = false

//global variables
const weather = Vue.observable({ weather: [] })
const days = Vue.observable({ days: 0 })
const overlay = Vue.observable({ overlay: true})


Object.defineProperty(Vue.prototype, '$weather', {
  get () {
    return weather.weather;
  },
  set (value) {
    weather.weather = value;
  }
})

Object.defineProperty(Vue.prototype, '$days', {
  get () {
    return days.days;
  },
  set (value) {
    days.days = value;
  }
})

Object.defineProperty(Vue.prototype, '$overlay', {
  get () {
    return overlay.overlay;
  },
  set (value) {
    overlay.overlay = value;
  }
})


new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
